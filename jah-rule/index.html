<!DOCTYPE html>
<html lang="en">
    <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta http-equiv="content-type" content="text/html; charset=utf-8">

      <!-- Enable responsiveness on mobile devices-->
      <!-- viewport-fit=cover is to support iPhone X rounded corners and notch in landscape-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, viewport-fit=cover">

      <title>Jean G3nie</title>

      <!-- CSS -->
      <link rel="stylesheet" href="/print.css" media="print">
      <link rel="stylesheet" href="/poole.css">
      <link rel="stylesheet" href="/hyde.css">
      <link rel="stylesheet" href="/hhl.css">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700|Abril+Fatface">

      
        <link rel="alternate" type="application/atom+xml" title="RSS" href="/atom.xml">
      

      
      
    </head>

    <body class=" ">
        
            <div class="sidebar">
                <div class="container sidebar-sticky">
                    <div class="sidebar-about">
                        
                            <a href="&#x2F;"><h1>Jean G3nie</h1></a>
                            
                            <p class="lead">Coder, tinkerer and maker</p>
                            
                        
                    </div>

                    <ul class="sidebar-nav">
                        
                        
                        <li class="sidebar-nav-item"><a href="&#x2F;about&#x2F;">About</a></li>
                        
                        <li class="sidebar-nav-item"><a href="&#x2F;gfret&#x2F;">Gfret</a></li>
                        
                        <li class="sidebar-nav-item"><a href="&#x2F;zterm&#x2F;">Zterm</a></li>
                        
                        <li class="sidebar-nav-item"><a href="&#x2F;vapad&#x2F;">Vapad</a></li>
                        
                        
                    </ul>

                    <div class=buttons>
                        <a href="&#x2F;atom.xml" title="Subscribe to blog">
                            <img src="&#x2F;rss.svg" width="48px" height="48px">
                        </a>
                        <a href="https://codeberg.org/jeang3nie/" title="Follow me on Codeberg" target="blank">
                            <img src="&#x2F;codeberg.svg" width="48px" height="48px">
                        </a>
                        <a href="https://social.linux.pizza/web/@jeang3nie" title="Connect on Mastodon" target="blank">
                            <img src="&#x2F;mastodon.svg" width="48px" height="48px">
                        </a>
                        <a href="https://liberapay.com/jeang3nie/" title="Support me on LeberaPay" target="blank">
                            <img src="&#x2F;liberapay.svg" width="48px" height="48px">
                        </a>
                    </div>
                </div>
            </div>
        

        <div class="content container">
          <dic class="content">
            
<div class="post">
  <h1 class="post-title">Jah Rule</h1>
  <p>Security is a multi-faceted subject in computing, and even amongst those who use
Unix in one form or another there are disagreements about how best to handle
various aspects of system administration. In today's blog post I'm going to be
talking about the subject of controlled priviledge escalation, and in particular
the presence of suid binaries on your system.<span id="continue-reading"></span></p>
<h2 id="a-brief-overview-of-unix-permissions">A (brief) overview of Unix permissions</h2>
<p>Unix permissions work due to a number which is stored along with each file representing
what actions may be taken on that file and by whom. These permissions are encoded
by looking at the actual bits which represent that number, which can best be
visualized by looking at a few numbers in binary form.</p>
<ul>
<li><strong>0b0001</strong> - <em>1</em></li>
<li><strong>0b0010</strong> - <em>2</em></li>
<li><strong>0b0011</strong> - <em>3</em></li>
<li><strong>0b0100</strong> - <em>4</em></li>
<li><strong>0b0101</strong> - <em>5</em></li>
<li><strong>0b0111</strong> - <em>6</em></li>
<li><strong>0b1000</strong> - <em>7</em></li>
</ul>
<p>In looking at just these numbers, and just considering the bits that happen to be
flipped <em>on</em>, we can see that the number three contains the bits of both 1 and 2,
while the bumber 5 contains the bits of 1 and 4. Unix permissions use a 32 bit
integer, allowing for 32 distinct boolean values to be stored in one integer. For
the sake of this article, the interesting bit is the one representing the <em>suid</em>
permission. When an executable has this bit flipped on, then that executable is
run with the effective user id of the owner of the file. If that owner happens to
be root, then a normal user can run that program with root level privilege.</p>
<p>It should go without saying that a suid binary is a powerful tool which is rife
with the possibility for abuse. If that program can be used to spawn other programs,
such as a shell, then the security implications can be absolute. Therefore it is
a good idea from a security standpoint to keep the number of suid binaries on
your system to a minimum and to make sure that the ones which do exist are well
designed and difficult to misuse.</p>
<p>As often happens to systems as they develop, the number of suid binaries on an
average Linux system has steadily increased over the years. You might expect to
have only one or two of them, <code>su</code> and <code>sudo</code> coming immediately to mind, but you
would likely be wrong. I just did a check on my laptop running Arch and found 25
suid binaries in /usr/bin, which is kind of scary to be honest. Some of them are
suid for reasons which are not immediately apparent. A CD burning program might
be installed suid in order to be able to write to the device file /dev/cd0,
although arguably the same effect could be had by setting the ownership of those
device files to a certain group and ensuring that every user needing access to
those programs is a member of that group. That and, you know, optical media is
not very common anymore. But I digress, it's absolutely overkill to have a suid
root owned binary for this purpose.</p>
<h2 id="the-big-two">The big two</h2>
<p>The big two of suid programs that you will find on <em>almost</em> any Unix machine are
<code>su</code> and <code>sudo</code>. The source of the <code>su</code> program might vary depending on if you
are running Linux, FreeBSD, or Solaris. It might also differ based on your actual
Linux distribution - case in point Alpine uses Busybox. But in all cases <code>su</code> is
a nice simple program. The suckless implementation in Ubase is 124 lines. </p>
<p>Now sudo on the other hand is absolutely <em>not</em> a simple program. In fact, checking
the latest release with tokei I get 108255 lines of C and 18747 lines of C headers.
There's also some 57860 lines of autoconf and a smattering of perl, among others.
Sudo allows fine grained access control and is highly configurable. It has had
a few security vulnerabilities over the years, but to me those aren't the big issue.</p>
<p>The issue that I have with sudo is that it's so overkill for what it does. Leading
from that, it has a relatively complex configuration file and is often, if not
regularly, configured in an insecure manner. Linux distributions started pushing
the use of sudo rather then su some years back because of the idea that you didn't
need a long running root shell to just run a couple of commands. That makes sense,
but frankly they were not thinking clearly when they put such a powerful and easy
to screw up program on every system, including systems being run by non technical
users who will never need even a fraction of it's features.</p>
<p>All you really need to do 99.999% of what sudo gets used for is a program that
can grant root privilege to a specific subset of users using a proper authentication
method. There is no need for a configuration file whatsoever. There is no reason to
support any means of authentication besides that used by the <code>login</code> command on
your system (passwords hashed via libcrypt). There is definitely no excuse for such
a program to be comprised of thousands of lines written in an unsafe language like
C.</p>
<p>What's even more frustrating is that the <code>su</code> command already was able to run arbitrary
commands via the <code>-c</code> command line flag, so a simple shell alias could arguably have
fit the bill, too.</p>
<h2 id="alternatives">Alternatives</h2>
<p>The OpenBSD folks released a sudo replacement a few years back called <code>doas</code>. OpenBSD
is a great system, and while you can run <code>doas</code> on other systems, some of the things
that make it especially secure (relatively speaking) on OPenBSD just don't translate
to other operating systems. For instance, the OpenBSD kernel has a system call which
allows one to stay authenticated for a period of time, after which privileges are
automatically dropped. This function has to be implemented using temporary files and
timestamps on other systems.</p>
<p>The <code>pkexec</code> program (part of the polkit package) is a part of certain modern
stacks (cough cough Gnome) and functions differently. It's a daemon which runs
in the background and you connect to it with a client to authenticate. My issue
is that computing cycles are not free, and why should we have a background process
running the entire time that you're logged in to facilitate the once in a while
need for increased privileges? That and, once again, it's an overly complex
system that one can just imagine having serious bugs lurking about. In fact there
was just such a vulnerability discovered in pkexec about a year ago aptly named
<a href="https://blog.qualys.com/vulnerabilities-threat-research/2022/01/25/pwnkit-local-privilege-escalation-vulnerability-discovered-in-polkits-pkexec-cve-2021-4034">pwnkit</a>.</p>
<p>There was a somewhat tongue in cheak blog post by a fairly well known Rust dev not
long ago that, unfortunately in my opinion, got taken a bit too seriously. This
blog post described the &quot;🥺&quot; program, designed to address the issue of sudo's
complexity by creating a brutally simple replacement. 🥺's security model is that
in order to run the program, one must be able to type an emoji, thus making it
unlikely that a bot will ever discover how to run the program. Thus it has no
authentication mechanism whatsoever. It also has no facility to run a program as
any user or group other than <code>root:root</code>. I will admit to loving simplicity, but
this security model is naive at best. However, it was an entertaining blog post
and it happened to be the inspiration for this one. The original post can be read
<a href="https://xeiaso.net/blog/%F0%9F%A5%BA">here</a>.</p>
<h2 id="jah-rule">Jah Rule</h2>
<p>Naming things is hard, right? Well, a program like sudo or doas allows a normal
user to act as if they were the superuser, who can do anything on your system. Kind
of like being God of your little Unix world. The Rastafarian name for God is <code>Jah</code>,
and being a Bob Marley fan I figured it might be nice to think of Bob when I run
my updates each day. So my sudo replacement is named <code>jah</code>.</p>
<p>Jah is written in Rust, for whatever that is worth. It's nowhere near as minimal
as &quot;🥺&quot; because I want it to be more than just a toy, but it's still an extremely
minimalist command line tool. The <code>Cargo.toml</code> file pulls in a total of four
direct dependencies. I'm using Clap because it's simply the best argument parser
in existence, but I have the feature set somewhat reduced. It also uses libc because
due to the nature of the beast the program has to make use of some interfaces not
provided by Rust's <code>std</code>. The <code>rpasswd</code> crate is used to hide passwords as they
are being typed, and finally the Rust bindings to libcrypt.</p>
<pre data-lang="Sh" style="background-color:#282828;color:#fdf4c1aa;" class="language-Sh "><code class="language-Sh" data-lang="Sh"><span style="color:#fdf4c1;">Run COMMAND as another user
</span><span>
</span><span style="color:#fdf4c1;">Usage: jah </span><span style="color:#fa5c4b;">[</span><span style="color:#fdf4c1;">OPTIONS</span><span style="color:#fa5c4b;">] </span><span style="color:#fe8019;">&lt;</span><span style="color:#fdf4c1;">COMMAND</span><span style="color:#fe8019;">&gt;</span><span style="color:#fdf4c1;">...
</span><span>
</span><span style="color:#fdf4c1;">Arguments:
</span><span>  </span><span style="color:#fe8019;">&lt;</span><span>COMMAND</span><span style="color:#fe8019;">&gt;</span><span>...
</span><span>
</span><span style="color:#fdf4c1;">Options:
</span><span>  </span><span style="color:#fdf4c1;">-u, --user </span><span style="color:#fe8019;">&lt;</span><span style="color:#fdf4c1;">user</span><span style="color:#fe8019;">&gt;</span><span style="color:#fdf4c1;">    The user that the command will run as </span><span style="color:#fa5c4b;">[</span><span style="color:#fdf4c1;">default: root</span><span style="color:#fa5c4b;">]
</span><span>  </span><span style="color:#fdf4c1;">-g, --group </span><span style="color:#fe8019;">&lt;</span><span style="color:#fdf4c1;">group</span><span style="color:#fe8019;">&gt;</span><span style="color:#fdf4c1;">  The group that the command will run as </span><span style="color:#fa5c4b;">[</span><span style="color:#fdf4c1;">default: wheel</span><span style="color:#fa5c4b;">]
</span><span>  </span><span style="color:#fdf4c1;">-h, --help           Print help (see more with </span><span style="color:#b8bb26;">&#39;--help&#39;</span><span>)
</span><span>  </span><span style="color:#fdf4c1;">-V, --version        Print version
</span></code></pre>
<p>Jah has no configuration file and the only options it accepts are a user or group,
if one wants to deviate from the default of <code>root:root</code>. I haven't bothered to
alter the environment used to run the command like sudo does with the exception
of changing the <code>HOME</code> variable to that of the new user. If that is a concern,
then one could always invoke it as <code>jah env -i &lt;command&gt;</code>. But I did take care,
unlike the above mentioned emoji named command, to make sure that the program's
stack has had temporary values and variables dropped before it <code>exec</code>s into the
desired command. Running <code>tokei</code> tells me that as of this writing jah is made up
of 420 lines of Rust, excluding external crates. The binary weighs in at 616k on
x86_64.</p>
<p>I could have made this much simpler, but I want it to be actually useful and not
annoying. Sudo will save the time of your last authentication and allow you to
run it again without authenticating for another five minutes. Jah does the same.
Instead of determining who can use the program via configuration file, any user
who is a member of the group <code>wheel</code> can run any program using jah after authenticating
with their own password. If even typing a password is too much for you, then you
can make a system group named 'jah' and add your user to it, and you will then
be able to run any command as any user and group without a password. If that makes
you uncomfortable (and it probably should) then simply do not have that group on
your machine.</p>
<blockquote>
<p>NOTE: The '<strong>wheel</strong>' group is historically for sysadmins and most <code>su</code> implementations
require a user to be a member of '<strong>wheel</strong>' in order to be allowed to use it.
Thus the design has an obvious historical precedent.</p>
</blockquote>
<p>Code for <code>jah</code> is at the <a href="https://codeberg.org/jeang3nie/jah">codeberg</a> repository.
It is intended to become a part of HitchHiker Linux in the near future.</p>

  <span class="post-date">2023-02-11</span>
</div>

        </div>

    </body>

</html>
